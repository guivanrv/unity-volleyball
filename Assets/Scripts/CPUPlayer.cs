﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CPUPlayer : Player
{
    [SerializeField] private Transform ball;

    override public Vector3 getImpulse() {
        float x = this.movementSpeed * -1;
        float z = this.movementSpeed * (Random.Range(0, 1) > 0.5f ? -1 : 1);
        Vector3 impulse = new Vector3(x, this.verticalForce, z);
        Debug.Log("CPU impulse "+ impulse.ToString());
        return impulse;
    }

    // Update is called once per frame
    void Update()
    {
     if (ball.position.x < transform.position.x) {
         MoveLeft();
     } else {
         MoveRight();
     }

      if (ball.position.z < transform.position.z) {
         MoveDown();
     } else {
         MoveUp();
     }

     if (Mathf.Abs(ball.position.x - transform.position.x) < 0.1) {
         Jump();
     }

     if (this.verticalForce != 0) {
            applyGravity();
     }
    }
}
