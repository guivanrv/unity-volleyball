﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    [SerializeField] private Transform cameraTransform;
    [SerializeField] private float timeToShake;
    private Vector3 defaultPosition;
    private float timeShaking = 0;
    private bool active = false;

    void Start() {
        defaultPosition = cameraTransform.position;
    }

    public void Shake() {
        if (!active) {
            active = true;
        } else {
            timeShaking = 0;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (active) {
            timeShaking+=Time.deltaTime;
            this.cameraTransform.position = this.cameraTransform.position + (cameraTransform.up * Mathf.Sin(66 * timeShaking)) * 0.1f;
        } else if (this.cameraTransform.position != this.defaultPosition) {
            this.cameraTransform.position = defaultPosition;
        }

        if (timeShaking >= timeToShake) {
            timeShaking = 0;
            active = false;
        }
        //Debug.Log(timeShaking+ " " + timeToShake);
    }

   
}
