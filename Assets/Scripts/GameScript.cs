﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public enum PlayerEnum {Player1, Player2};

public class GameScript : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI leftScore, rightScore;

    private int leftPlayerScore = 0;
    private int rightPlayerScore = 0;
    [SerializeField] private AudioSource countdown;
    [SerializeField] private AudioSource bounce;
    [SerializeField] private AudioSource explosion;
    
    private CameraShake cam;

    // Start is called before the first frame update
    void Start()
    {
        cam = GetComponent<CameraShake>();
    }

    public void BallTouchedTheGround(PlayerEnum player) {
      switch (player)
      {
        case PlayerEnum.Player1:
            rightPlayerScore++;
            break;
        case PlayerEnum.Player2:
            leftPlayerScore++;
            break;
      }

      cam.Shake();
      this.explosion.Play();
      PlayCountdownSound();
    }

    void PlayCountdownSound() {
        this.countdown.Play();
    }

    public void PlayerBouncedTheBall() {
        cam.Shake();
        this.bounce.Play();
    }

    // Update is called once per frame
    void Update()
    {
        this.leftScore.text = leftPlayerScore + "";
        this.rightScore.text = rightPlayerScore + "";
    }
}
