﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private KeyCode left;
    [SerializeField] private KeyCode right;
    [SerializeField] private KeyCode up;
    [SerializeField] private KeyCode down;
    [SerializeField] private KeyCode jump;

    [SerializeField] protected float movementSpeed = 0.05f;
    [SerializeField] protected float MIN_X;
    [SerializeField] protected float MAX_X;
    [SerializeField] protected float MIN_Z;
    [SerializeField] protected float MAX_Z;

    [SerializeField] protected AudioSource audioSource;
    
    protected float verticalForce = 0;
    protected float jumpForce = 0.5f;
    protected float GROUND_Y = 19.15f;
    protected float gravity = 0.04f;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKey(left)) {
            MoveLeft();
        }

        if (Input.GetKey(right)) {
            MoveRight();
        }
        
        if (Input.GetKey(up)) {
            MoveUp();
        }

        if (Input.GetKey(down)) {
            MoveDown();
        }

        if (Input.GetKeyDown(jump)) {
            Jump();
        }

        if (this.verticalForce != 0) {
            applyGravity();
        } else {
            if (this.transform.position.y != GROUND_Y) {
                this.transform.position = new Vector3(this.transform.position.x, GROUND_Y, this.transform.position.z);
            }
        }
    }

    private bool isMovingX() {
        return Input.GetKey(left) || Input.GetKey(right); 
    }

    private bool isMovingZ() {
        return Input.GetKey(up) || Input.GetKey(down); 
    }

    public virtual Vector3 getImpulse() {
        float x = isMovingX() ? this.movementSpeed * (Input.GetKey(left) ? -1 : 1) : 0;
        float z = isMovingZ() ? this.movementSpeed * (Input.GetKey(down) ? -1 : 1) : 0;
        return new Vector3(x, this.verticalForce, z);
    }



    protected void MoveUp() {
        if (this.transform.position.z < MAX_Z)
                this.transform.position += (this.transform.forward * movementSpeed);
    }
    
    protected void MoveDown() {
         if (this.transform.position.z > MIN_Z)
                this.transform.position += (this.transform.forward * -movementSpeed);
    }

    protected void MoveLeft() {
        if (this.transform.position.x > MIN_X)
                this.transform.position += (this.transform.right * -1 * movementSpeed);
    }
    protected void MoveRight() {
        if (this.transform.position.x < MAX_X)
            this.transform.position += (this.transform.right * movementSpeed);
    }
    protected void Jump() {
        if (Mathf.Abs(this.transform.position.y - GROUND_Y) < 0.05 && this.verticalForce != (jumpForce - gravity)){
            this.verticalForce = jumpForce;
            audioSource.Play();
        } else {
            //Debug.Log("#Jump canceled " + (this.transform.position.y == GROUND_Y) + " " + this.transform.position.y + " " +  GROUND_Y + " " + this.verticalForce);
        }
    }
    
    protected void applyGravity() {

        if (this.transform.position.y <= GROUND_Y && this.verticalForce < 0) {
            this.transform.position = new Vector3(this.transform.position.x, GROUND_Y, this.transform.position.z);
            this.verticalForce = 0;
        }

        this.verticalForce -= this.gravity;
        this.transform.position += this.transform.up * this.verticalForce;
    }
}
