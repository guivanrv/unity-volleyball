﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class TouchedTheGroundEvent : UnityEvent<PlayerEnum>
{
}

public class Ball : MonoBehaviour
{
    private float sphereRadius = 0.5f;
    private float verticalVelocity = 0f;
    private float horizontalVelocity = 0f;
    private float horizontalVelocityZ = 0f;

    private float GRAVITY_VALUE = 0.003f;
    private float gravity = 0.003f;


    private Rigidbody rb;
    private Vector3 defaultPosition;

    [SerializeField] private TouchedTheGroundEvent onGroundCollision;
    [SerializeField] private UnityEvent playerBouncedTheBall;
    [SerializeField] private ParticleSystem assist;
    [SerializeField] private ParticleSystem explosion;
    [SerializeField] private float bounce = 0.05f;
    [SerializeField] private float horizontalSpeed = 0.29f, horizontalSpeedZ = 0.056f;


    void Start() {
        this.defaultPosition = this.transform.position;
        this.rb = GetComponent<Rigidbody>();

        ResetGame();

        explosion.Stop();
    }


    public void ResetGame() {
        this.horizontalVelocity = 0f;
        this.horizontalVelocityZ = 0f;
        this.verticalVelocity = 0f;
        this.transform.position = defaultPosition;
        gravity=0f;
        this.Invoke("ResetPosition", 3f);
        explosion.Play();
    }
    public void ResetPosition() {
        this.transform.position = defaultPosition;
        this.horizontalVelocity = Random.Range(-horizontalSpeed, horizontalSpeed);
        this.horizontalVelocityZ = Random.Range(-horizontalSpeedZ, horizontalSpeedZ);
        this.verticalVelocity = 0;
        gravity = GRAVITY_VALUE;
    }

    void OnTriggerEnter(Collider collider)
    {
        this.verticalVelocity = bounce;
        switch (collider.gameObject.tag) {
            case "Wall":
            case "WallX":
            case "WallZ":
             this.horizontalVelocity = (this.horizontalVelocity * Random.Range(0.78f, 0.88f));
             this.horizontalVelocityZ = (this.horizontalVelocityZ * Random.Range(0.78f, 0.88f));
             if (collider.gameObject.tag == "WallX") {
                 this.horizontalVelocity *= -1;
             } else if (collider.gameObject.tag == "WallZ") {
                 this.horizontalVelocityZ *= -1;
             } else {
                 this.verticalVelocity *= -1;
             }

             //Debug.Log("Wall Collision " + horizontalVelocity + " " + horizontalVelocityZ);
             break;
            case "Player": 
                //Debug.Log(collider.gameObject.tag);
                Player player = collider.gameObject.GetComponent<Player>();
                Vector3 collisionImpulse = player.getImpulse();

                if (Mathf.Abs(collisionImpulse.x) > 0.24) {
                    this.horizontalVelocity = collisionImpulse.x;
                } else {
                    this.horizontalVelocity = -(this.horizontalSpeed * Random.Range(0.90f, 0.92f));
                }

                if (Mathf.Abs(collisionImpulse.z) > 0.2) {
                    this.horizontalVelocityZ = collisionImpulse.z;
                } else {
                    this.horizontalVelocityZ = -(this.horizontalSpeedZ * Random.Range(0.92f, 0.95f));
                }
                //Debug.Log("Player Collision " + horizontalVelocity + " " + horizontalVelocityZ);
                playerBouncedTheBall.Invoke();
                break;
            case "Net":
                //Debug.Log("Net");
                break;
            default: 
                onGroundCollision.Invoke(this.transform.position.x < this.defaultPosition.x ? PlayerEnum.Player1 : PlayerEnum.Player2 );

                ResetGame();
                //Debug.Log("Gotta be ground");
                break;
        }
    }

    void Update() {
        applyVelocities();
        assist.transform.position = new Vector3(transform.position.x, assist.transform.position.y, transform.position.z);

        if (Input.GetKeyDown(KeyCode.R)) {
            ResetPosition();
        }
    }

    void applyVelocities() {
        Vector3 position = this.transform.position;
        
        this.verticalVelocity -= gravity;
        position += this.transform.up * this.verticalVelocity;

        Vector3 horizontalSpeed = this.transform.right * this.horizontalVelocity;
        position += horizontalSpeed;
        
        Vector3 horizontalSpeedZ = this.transform.forward * this.horizontalVelocityZ;
        position += horizontalSpeedZ;
        
        this.transform.position = position;
        
        //air drag to slow horizontal velocity down
        position -= horizontalSpeed * 0.02f;
    }
}
